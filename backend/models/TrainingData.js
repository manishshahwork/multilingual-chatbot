const mongoose = require("mongoose");

const TrainingDataSchema = mongoose.Schema({
    log_date: { type: Number },
    question: { type: String },
    answer: { type: String },
    feedback: { type: String, default:"" },
    sme: { type: String, default:"" },
    is_answered: {type: Boolean},
    is_liked: {type:Boolean}
});

const TrainingData = module.exports = mongoose.model("TrainingData", TrainingDataSchema, "trainingdata");