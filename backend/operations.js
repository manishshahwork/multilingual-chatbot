const mongoose = require("mongoose");
const ItemLog = require('./models/ItemLog');
const TrainingData = require('./models/TrainingData');

class Operations {

    //Item Log specfic functions
    static getItemLogById(id) {
        console.log("id:" + id);
        ItemLog.findById(id, function (err, data) {
            if (err) {
                return null;
            } else {
                console.log("returning data: " + data);
                return data;
            }
        });
    }

    static getAllItemLogs() {
        return ItemLog.find();
    }

    static getItemLogByLogDate(logDate, callback) {
        let query = ItemLog.find();
        query.where('log_date').equals(logDate);
        // query.where('log_date').gte(logDate);
        // query.where('log_date').lte(logDate);
        ItemLog.findOne(query, callback);
    }

    static addItemLog(newItemLog, callback) {
        return newItemLog.save(callback);
    }

    static addManyItemLogs(itemLogsArr) {
        return ItemLog.insertMany(itemLogsArr);
    }

    static updateManyItemLogs(itemLogsArr) {
        console.log("Item Logs: updateManyItemLogs: ");
        for (let k = 0; k < itemLogsArr.length; k++) {
            // console.log(trainingDataArr[k]);
            var mod = new ItemLog(itemLogsArr[k]);
            ItemLog.findOneAndUpdate({ '_id': itemLogsArr[k]._id }, mod, function (err, c) {
                if (err) {
                    console.log(err);
                }
            });
        }
        return "SUCCESS in updating item logs..";
    }

    static deleteItemLog(id) {
        console.log("id::::" + id);
        return ItemLog.findOneAndRemove(id);
    }


    static getBooleanValueFromNumber(n) {
        if (n == 0) return false;
        if (n == 1) return true;
    }


    //search
    static getSpecificItemLogs(searchStartTime, searchEndTime, searchFeedbackType, searchAnswerType) {
        let query = ItemLog.find();
        if (searchStartTime != -1) query.where('log_date').gte(searchStartTime);
        if (searchEndTime != -1) query.where('log_date').lte(searchEndTime);
        if (searchFeedbackType != -1) query.where('is_liked').equals(this.getBooleanValueFromNumber(searchFeedbackType));
        if (searchAnswerType != -1) query.where('is_answered').equals(this.getBooleanValueFromNumber(searchAnswerType));

        //execute the query
        return query.exec();
    }


    static setItemLogLikeDislike(itemLogId, isLike) {
        ItemLog.findByIdAndUpdate({ '_id': itemLogId }, { 'is_liked': isLike }, function (err, c) {
            if (err) {
                console.log(err);
                return err;
            } else {
                return c;
            }
        });

    }

    ////////////////////// Training Data Specific

    static getAllTrainingData() {
        return TrainingData.find();
    }

    static addManyTrainingData(trainingDataArr) {
        return TrainingData.insertMany(trainingDataArr);
    }

    //search
    static getSpecificTrainingData(searchStartTime, searchEndTime, searchFeedbackType, searchAnswerType) {
        let query = TrainingData.find();
        if (searchStartTime != -1) query.where('log_date').gte(searchStartTime);
        if (searchEndTime != -1) query.where('log_date').lte(searchEndTime);
        if (searchFeedbackType != -1) query.where('is_liked').equals(this.getBooleanValueFromNumber(searchFeedbackType));
        if (searchAnswerType != -1) query.where('is_answered').equals(this.getBooleanValueFromNumber(searchAnswerType));

        //execute the query
        return query.exec();
    }

    //search answers for questions
    static getSpecificAnswers(question) {
        let query = TrainingData.find();
        query.where('question').equals({ $regex: '.*' + question + '.*', $options: 'i' });
        //execute the query
        return query.exec();
    }

    static updateManyTrainingData(trainingDataArr) {
        console.log("Training Data updateManyTrainingData: ");
        // console.log(trainingDataArr);
        for (let k = 0; k < trainingDataArr.length; k++) {
            // console.log(trainingDataArr[k]);
            var td = new TrainingData(trainingDataArr[k]);
            // console.log("will update::: ");
            // console.log(td._id);
            TrainingData.findOneAndUpdate({ '_id': trainingDataArr[k]._id }, td, function (err, c) {
                // console.log("c:::'");
                // console.log(c);
                if (err) {
                    console.log(err);
                }
            });
        }
        return "SUCCESS in updating training data..";
    }

    static addTrainingData(newTrainingData, callback) {
        return newTrainingData.save(callback);
    }

    // static getItemLogByLogDate(logDate, callback) {
    //     let query = ItemLog.find();
    //     query.where('log_date').equals(logDate);
    //     // query.where('log_date').gte(logDate);
    //     // query.where('log_date').lte(logDate);
    //     ItemLog.findOne(query, callback);
    // }

    // static addItemLog(newItemLog, callback) {
    //     newItemLog.save(callback);
    // }


    // //search
    // static getSpecificItemLogs(searchStartTime, searchEndTime, searchFeedbackType, searchAnswerType) {
    //     let query = ItemLog.find();
    //     if (searchStartTime != -1) query.where('log_date').gte(searchStartTime);
    //     if (searchEndTime != -1) query.where('log_date').lte(searchEndTime);
    //     if (searchFeedbackType != -1) query.where('is_liked').equals(this.getBooleanValueFromNumber(searchFeedbackType));
    //     if (searchAnswerType != -1) query.where('is_answered').equals(this.getBooleanValueFromNumber(searchAnswerType));

    //     //execute the query
    //     return query.exec();

    // }

    // static addManyItemLogs(itemLogsArr) {
    //     return ItemLog.insertMany(itemLogsArr);
    // }

    // static updateManyItemLogs(itemLogsArr) {
    //     return ItemLog.updateMany(itemLogsArr);
    // }
    // static deleteItemLog(id) {
    //     console.log("id::::" + id);
    //     return ItemLog.findOneAndRemove(id);
    // }

}
module.exports = Operations;