const express = require("express");
const router = express.Router();
const Operations = require("../operations");
const TrainingData = require("../models/TrainingData")


function logMsg(msg) {
    console.log("Training Data Route: " + msg);
}
function logObj(obj) {
    console.log("Training Data Route: ");
    console.log(obj);
}

//get all training data
router.get("/getAll", async function (req, res) {
    logMsg("trainingData-routes: getAll");
    logObj(req.body);
    try {
        let trainingData = await Operations.getAllTrainingData();
        res.status(200).json({
            status: 200, message: 'Success in getting item', data: trainingData
        });
    } catch (err) {
        res.status(500).json({
            status: 400, message: 'Error in getting item', data: err
        });
    }
});

//getMatchingAnswers
router.get("/getMatchingAnswers/:question", async function (req, res) {
    logMsg("trainingData-routes: getMatchingAnswers");
    logObj(req.body);
    logObj(req.params);
    try {
        let trainingData = await Operations.getSpecificAnswers(req.params.question);
        res.status(200).json({
            status: 200, message: 'Success in getting item', data: trainingData
        });
    } catch (err) {
        res.status(500).json({
            status: 400, message: 'Error in getting item', data: err
        });
    }
});


//add array of training data
router.post("/addMany", async function (req, res) {
    logMsg("Many Training data to be added ");
    logObj(req.body);
    logObj(req.params);

    try {
        await Operations.addManyTrainingData(req.body).then(reply => {
            logMsg("Training Data added succesfully");
            logObj(reply);
            res.status(200).json({
                status: 200, message: 'Success in adding training data'
            });
        });
    } catch (err) {
        logMsg("ERROR in adding training data... ");
        logObj(err);
        res.json({ success: false, msg: 'Error in adding training data: ' + err });
    }


    // await Operations.addManyTrainingData(req.body, function (err) {
    //     if (err) {
    //         console.log("error in adding training data ");
    //         console.log(err);
    //         res.status(200).json({
    //             status: 200, message: 'Error in adding training'
    //         });
    //     } else {
    //         console.log("success in adding. ");
    //         res.status(200).json({
    //             status: 200, message: 'Training data are added successfully'
    //         });
    //     }
    // });
});

router.post("/updateManyTrainingData", async function (req, res) {
    logMsg("inside update many training data");
    logObj(req.body);
    logObj(req.params);

    try {
        await Operations.updateManyTrainingData(req.body)
        logMsg("success in updating. ");
        res.status(200).json({
            status: 200, message: 'Success in updating training data'
        });
        // await Operations.updateManyTrainingData(req.body).then(reply => {
        //     logMsg("success in updating. ");
        //     logObj(reply);
        //     res.status(200).json({
        //         status: 200, message: 'Success in updating training data'
        //     });
        // });
    } catch (err) {
        logMsg("ERROR in updating... ");
        logObj(err);
        res.json({ success: false, msg: 'Error in updating training data: ' + err });
    }


    // await Operations.updateManyItemLogs(req.body, function (err) {
    //     console.log("returnnnnn");
    //     if (err) {
    //         console.log("error in updating. ");
    //         console.log(err);
    //         res.json({ success: false, msg: 'Error in updating item logs: ' + err });
    //     } else {
    //         console.log("success in adding. ");
    //         res.json({ success: true, msg: 'Item log updating successful: ' });
    //     }
    // });
})


// //update itemLog
// router.put("/update", function (req, res) {
//     console.log("Update item log");
//     console.log(req.body);
//     res.send("Item log Updated.");
// });

//delete training data
router.post("/deleteOne", async function (req, res) {
    logMsg("Deleting training data");
    logObj(req.body);
    logObj(req.params);
    let id = req.body._id;
    try {
        if(id == null || id == "" || id == undefined){
            throw error("Invalid id... (" + id + ")");
        }
        await TrainingData.findByIdAndDelete(id);
        logMsg("Training Data deleted.");
        res.status(200).json({
            status: 200, message: 'Success in deleting'
        });
    } catch (error) {
        res.status(500).json({
            status: 500, message: 'Error in deleting', data: error
        });
    }

})

// returns specific training data
router.get('/search/:searchStartTime/:searchEndTime/:searchFeedbackType/:searchAnswerType', async function (req, res) {
    logMsg("Specific search for Training Data");
    logObj(req.body);
    logObj(req.params);
    try {
        let result = await Operations.getSpecificTrainingData(req.params.searchStartTime, req.params.searchEndTime, req.params.searchFeedbackType, req.params.searchAnswerType);
        logMsg("Success in search.");
        logObj(result);
        res.status(200).json({
            status: 200, message: 'Success in searching', data: result
        });
    } catch (err) {
        logMsg("Error in searching training data.");
        logObj(err);
        res.status(500).json({
            status: 500, message: 'Error in getting item', data: err
        });
    }
});


// //get itemLog by id
// router.get("/getById/:id", async function (req, res) {
//     console.log("getById");
//     console.log(req.body);
//     console.log(req.params);
//     let id = req.params.id;
//     try {
//         if (id == null) throw error("ID invalid");
//         ItemLog.findById(id, function (err, result) {
//             if (err) {
//                 res.status(500).json({
//                     status: 500, message: 'Error in getting item', data: err
//                 });
//             } else {
//                 res.status(200).json({
//                     status: 200, message: 'Success in getting item', data: result
//                 });
//             }
//         })
//     } catch (err) {
//         res.status(500).json({
//             status: 500, message: 'Error in getting item', data: err
//         });
//     }
// });

//


//add trainingData
router.post("/add", function (req, res) {
    logMsg("Training Data to be added ");
    logObj(req.body);
    logObj(req.params);
    let newTrainingData = new TrainingData(req.body);
    logMsg("About to save training data:");
    logObj(newTrainingData);
    try {
        Operations.addTrainingData(newTrainingData).then(reply => {
            logMsg("Training data added succesfully");
            logObj(reply);
            res.status(200).json({
                status: 200, message: 'Success in adding Training data'
            });
        });
    } catch (err) {
        logMsg("ERROR in adding Training data... ");
        logObj(err);
        res.status(500).json({
            status: 500, message: 'Error in adding Training data'
        });
    }
});


module.exports = router; 