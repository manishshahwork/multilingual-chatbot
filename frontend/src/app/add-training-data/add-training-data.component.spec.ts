import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddTrainingDataComponent } from './add-training-data.component';

describe('AddTrainingDataComponent', () => {
  let component: AddTrainingDataComponent;
  let fixture: ComponentFixture<AddTrainingDataComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddTrainingDataComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddTrainingDataComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
