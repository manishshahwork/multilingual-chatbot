import { Component, OnInit, EventEmitter } from '@angular/core';
import { TrainingData } from '../models/TrainingData.model';
import { DbService } from '../service/db.service';

@Component({
  selector: 'app-add-training-data',
  templateUrl: './add-training-data.component.html',
  styleUrls: ['./add-training-data.component.css']
})
export class AddTrainingDataComponent implements OnInit {

  public event: EventEmitter<any> = new EventEmitter();
  trainingData: TrainingData;

  constructor(private _db:DbService) { }

  ngOnInit() {
    this.trainingData = new TrainingData();
  }

  onClickAddTrainingData(){
    console.log(this.trainingData);
    this._db.addTrainingData(this.trainingData).subscribe(
      data=>{
        console.log("Successfully added training data");
        this.event.emit({msg: "add"});
      },
      err =>{
        console.log("Error in adding training data.");
        console.log(err);
      }
    );    
  }

  onClickCancel(){
    console.log(this.event);
    this.event.emit({data: "cancel"});
  }

}
