import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DashboardComponent } from './dashboard/dashboard.component';
import { ItemLogsDashboardComponent } from './item-logs-dashboard/item-logs-dashboard.component';
import { UiComponent } from './ui/ui.component';
import { ChatbotComponent } from './chatbot/chatbot.component';
import { AddTrainingDataComponent } from './add-training-data/add-training-data.component';
import { TicketComponent } from './ticket/ticket.component';

const routes: Routes = [
  { path: 'dashboard', component: DashboardComponent },
  { path: 'itemLogsDashboard', component: ItemLogsDashboardComponent },
  { path: 'ui', component: UiComponent },
  { path: 'chatbot', component: ChatbotComponent },
  { path: 'ticket', component: TicketComponent },
  { path: '', redirectTo: 'ui', pathMatch: 'full' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
