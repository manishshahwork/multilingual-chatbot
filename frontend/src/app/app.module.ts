import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ItemLogsDashboardComponent } from './item-logs-dashboard/item-logs-dashboard.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { HttpClientModule } from '@angular/common/http';
import { PapaParseModule } from 'ngx-papaparse';
import { FormsModule } from '@angular/forms';
import { UiComponent } from './ui/ui.component';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { ChatbotComponent } from './chatbot/chatbot.component';
import { TypeaheadModule } from 'ngx-bootstrap/typeahead';
import { InlineEditorModule } from '@qontu/ngx-inline-editor';
import { ModalModule } from 'ngx-bootstrap/modal';
import { AddTrainingDataComponent } from './add-training-data/add-training-data.component';
import { DataTableModule } from "angular-6-datatable";
import { ChatbotMenuComponent } from './chatbot-menu/chatbot-menu.component';
import { TicketComponent } from './ticket/ticket.component';
import { CreateTicketComponent } from './ticket/create-ticket/create-ticket.component';

@NgModule({
  declarations: [
    AppComponent,
    ItemLogsDashboardComponent,
    DashboardComponent,
    UiComponent,
    ChatbotComponent,
    AddTrainingDataComponent,
    ChatbotMenuComponent,
    TicketComponent,
    CreateTicketComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    PapaParseModule,
    FormsModule,
    BsDatepickerModule.forRoot(),
    TypeaheadModule.forRoot(),
    NgbModule.forRoot(),
    InlineEditorModule,
    ModalModule.forRoot(),
    DataTableModule
  ],
  providers: [],
  bootstrap: [AppComponent],
  entryComponents: [
    AddTrainingDataComponent,
    ChatbotMenuComponent,
    CreateTicketComponent
  ]
})
export class AppModule { }
