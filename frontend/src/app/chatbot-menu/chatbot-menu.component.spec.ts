import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChatbotMenuComponent } from './chatbot-menu.component';

describe('ChatbotMenuComponent', () => {
  let component: ChatbotMenuComponent;
  let fixture: ComponentFixture<ChatbotMenuComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChatbotMenuComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChatbotMenuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
