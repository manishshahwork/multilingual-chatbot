import { Component, OnInit, EventEmitter, ElementRef } from '@angular/core';
import { DbService } from '../service/db.service';
import { TrainingData } from '../models/TrainingData.model';
import { ItemLog } from '../models/ItemLog.model';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { ChatbotMenuComponent } from '../chatbot-menu/chatbot-menu.component';
import { AddTrainingDataComponent } from '../add-training-data/add-training-data.component';
import { NluService } from '../service/nlu.service';

@Component({
  selector: 'app-chatbot',
  templateUrl: './chatbot.component.html',
  styleUrls: ['./chatbot.component.css']
})
export class ChatbotComponent implements OnInit {
  chatHistoryArr = [];

  selected_language = "english";

  //icons
  ICON_BOT = "assets/core2/support.png";
  ICON_USER = "assets/core2/assistant.png";

  questionAsked: any;//questions selected
  suggestedItemLogQuestionsArr = []; //typeahead
  // firstMatchingAnswer: any;//matching answers for the question asked
  currentMatchingAnswer: string;//current answer for the question (need for log)
  DEFAULT_UNASNWERED_MESSAGE = "Sorry! Can you please rephrase your question ?";

  public eventEmitter: EventEmitter<any> = new EventEmitter();

  constructor(private _db: DbService, private _nlu: NluService, private modalService: BsModalService) { }

  ngOnInit() {
    this.fillupSuggestedArr();
    // this.firstMatchingAnswer = "";
    this.pushAnswer("How can I help you ?", "");
  }

  fillupSuggestedArr() {
    this._db.getAllTrainingData().subscribe(
      data => {
        console.log("Printing...");
        let itemLogsArr = data['data'];
        for (let k in itemLogsArr) {
          // console.log(itemLogsArr[k]);
          this.suggestedItemLogQuestionsArr.push(itemLogsArr[k].question);
        }
      },
      err => {
        console.log(err);
      }
    )
  }

  focusCursorToEnd() {
    var contentEditable = document.querySelector('div[contenteditable]');
    console.log(contentEditable);
    contentEditable.addEventListener('input', function (e) {

      // this function will be called after the current call-stack
      // is finished, at that time the user input will be inserted
      // in the content editable
      setTimeout(function () {

        // // save the caret position

        // var text = contentEditable.innerText;
        // // perform all your replacement on `text`
        // contentEditable.innerHTML = text;

        // set the caret position back
      }, 0)
    }
    )

  }

  onClickQuestion(event) {
    if (event.key == "Enter") {

      console.log("Search answers for question: " + this.questionAsked + " in language:" + this.selected_language);
      if (this.questionAsked == null || this.questionAsked.trim() == "")
        return;

      //for chat history save the question asked
      this.pushQuestion(this.questionAsked);

      //get answer from NLU
      this.getAnswersFromNLUAndPopulate();

      //get answer from database
      // this.getAnswersFromDbAndPopulate();

    }
  }

  async getAnswersFromNLUAndPopulate() {
    let result: any;
    try {
      let replyFromNLU = await this._nlu.getAnswerForQuestion(this.questionAsked, this.selected_language).toPromise();
      console.log("replyFromNLU:::::  " + replyFromNLU);
      result = replyFromNLU["response"];
    } catch (error) {
      result = "Error! Kindly check the logs.";
      console.log("error::");
      console.log(error);
    }

    console.log("result:::::  " + result);
    //build training data
    let tr = new TrainingData();
    tr.question = this.questionAsked;
    tr.answer = result;
    let trArr = [];
    trArr.push(tr);
    this.populateAnswers(trArr);



    // this._nlu.getAnswerForQuestion(this.questionAsked).subscribe(
    //   data => {
    //     console.log("#### SUCCESS reply from nlu ####");
    //     console.log(data);
    //     console.log("Response:: " + data["response"]);

    //     //build training data
    //     let tr = new TrainingData();
    //     tr.question = this.questionAsked;
    //     tr.answer = data["response"];
    //     let trArr = [];
    //     trArr.push(tr);
    //     this.populateAnswers(trArr);

    //     // //populate chatbot with received answer(s)
    //     // this.populateAnswers(data['data']);


    //   },
    //   err => {
    //     console.log("#### ERROR from nlu ####");
    //     console.log(err);
    //     let tr = new TrainingData();
    //     tr.question = this.questionAsked;
    //     tr.answer = err;
    //     let trArr = [];
    //     trArr.push(tr);
    //     this.populateAnswers(trArr);
    //   }
    // );


  }


  getAnswersFromDbAndPopulate() {
    this._db.getMatchingAnswers(this.questionAsked).subscribe(
      data => {
        console.log(data);
        //populate chatbot with received answer(s)
        this.populateAnswers(data['data']);

      },
      err => {
        console.log(err);
      }
    );
  }

  populateAnswers(trainingDataArr: TrainingData[]) {


    this.currentMatchingAnswer = "";
    console.log("trainingDataArr.length::: " + trainingDataArr.length);
    if (trainingDataArr.length <= 0) {
      this.currentMatchingAnswer = this.DEFAULT_UNASNWERED_MESSAGE;
      // console.log("curr::: " + this.currentMatchingAnswer);
    } else {
      for (let k = 0; k < trainingDataArr.length; k++) {
        this.currentMatchingAnswer += trainingDataArr[k].answer;
        // this.firstMatchingAnswer += "\nR## " + trainingDataArr[k].answer;
      }
    }
    //log this answer and question in database
    this.logResult();
    this.questionAsked = "";
    this.focusCursorToEnd();

  }

  /**
   * Log result in database (ItemLogs)
   */
  async logResult() {
    console.log("will log: question: '" + this.questionAsked + "' and answer:'" + this.currentMatchingAnswer + "'");
    let itemLog = new ItemLog();
    itemLog.log_date = Date.now();
    itemLog.question = this.questionAsked;
    itemLog.answer = this.currentMatchingAnswer;
    if (this.currentMatchingAnswer == this.DEFAULT_UNASNWERED_MESSAGE)
      itemLog.is_answered = false;
    else
      itemLog.is_answered = true;
    await this._db.addItemLog(itemLog).subscribe(
      data => {
        console.log("Log reply from server:");
        console.log(data);
        console.log("item log id: " + data['data']._id);
        //for chat history push the answer 
        this.pushAnswer(this.currentMatchingAnswer, data['data']._id);
      },
      err => {
        console.log("err:" + err);
      }
    );
  }


  pushQuestion(question) {
    this.chatHistoryArr.push({
      "originalMessage": question,
      "text": ' <img src="' + this.ICON_BOT + '" width="24" height="24">' + question + "</div>",
      // "text": '<i class="material-icons" style="font-size:48px;color:red">message</i> ' +  question,
      "itemLogId": "",
      "machine": false
    })
  }


  pushAnswer(answer, itemLogId) {
    this.chatHistoryArr.push({
      "originalMessage": answer,
      "text": answer + ' <img src="' + this.ICON_USER + '" width="24" height="24">',
      "itemLogId": itemLogId,
      "machine": true
    })
  }

  onClickLike(itemLogId, isLike) {
    console.log("Click like...." + itemLogId + " like? " + isLike);
    if (itemLogId == null || itemLogId == "") return;
    this._db.setLikeDislikeForItemLog(itemLogId, isLike).subscribe(data => {
      console.log('Particular item log updated...');
      console.log(data);
    },
      err => {
        console.log("Error in updating item log like");
        console.log(err);
      }
    );
  }

  onCloseChatbot() {
    console.log("close...");
    console.log(this.eventEmitter);
    this.eventEmitter.emit({ msg: "close" });
  }

  getPlainQuestionAnswerHistory() {
    let plainQuestionAnswerHistory = "";
    for (let k = 0; k < this.chatHistoryArr.length; k++) {
      if (this.chatHistoryArr[k].machine == true) {
        plainQuestionAnswerHistory += "<br>Machine::: " + this.chatHistoryArr[k].originalMessage;
      } else {
        plainQuestionAnswerHistory += "<br>User::: " + this.chatHistoryArr[k].originalMessage;
      }
    }
    return plainQuestionAnswerHistory;
  }

  printChatConversation(): void {
    let printContents, popupWin;
    // printContents = document.getElementById('print-section').innerHTML;
    popupWin = window.open('', '_blank', 'top=0,left=0,height=100%,width=auto');
    popupWin.document.open();
    console.log(this.chatHistoryArr);
    let plainQuestionAnswerHistory = this.getPlainQuestionAnswerHistory();
    console.log("plain::: " + plainQuestionAnswerHistory);
    printContents = plainQuestionAnswerHistory;
    // popupWin.document.write("<u>Chats as on date: '" + new Date(Date.now()) + "'</u><br>");
    // popupWin.document.write(plainQuestionAnswerHistory);
    popupWin.document.write(`
      <html>
        <head>
          <title>Chat Conversation</title>
          <style>
          //........Customized style.......
          </style>
        </head>
    <body onload="window.print();window.close()">${printContents}</body>
      </html>`
    );
    popupWin.document.close();
  }


  printRecentChatHistory() {
    this._db.getAllItemLogs().subscribe(
      data => {
        // console.log(data);
        let plainQuestionAnswerHistory = "";
        let itemLogsArr = data['data'];
        for (let k = 0; k < itemLogsArr.length; k++) {
          plainQuestionAnswerHistory += "<br>User::: " + itemLogsArr[k].question;
          plainQuestionAnswerHistory += "<br>Machine::: " + itemLogsArr[k].answer;
        }
        // console.log("plain::: " + plainQuestionAnswerHistory);
        let popupWin = window.open('', '_blank', 'top=0,left=0,height=100%,width=auto');
        popupWin.document.open();
        let printContents = plainQuestionAnswerHistory;
        popupWin.document.write(`
          <html>
            <head>
              <title>Recent Chat History</title>
              <style>
              //........Customized style.......
              </style>
            </head>
        <body onload="window.print();window.close()">${printContents}</body>
          </html>`
        );
        popupWin.document.close();
      },
      err => {
        console.log(err);
      }
    )

  }

  bsModalRef3: BsModalRef;
  openModalWithComponent() {
    const initialState = {
      list: [
      ],
      title: 'My Menu'
    };
    this.bsModalRef3 = this.modalService.show(ChatbotMenuComponent, { initialState });
    console.log("uiLbsmodalref: ");
    console.log(this.bsModalRef3);
    console.log("uiL bsmodalref: " + this.bsModalRef3.content.eventEmitter);
    this.bsModalRef3.content.closeBtnName = 'Close';

    this.bsModalRef3.content.eventEmitter.subscribe(data => {
      console.log('Child component\'s event was triggered', data);
      if (data.msg == "printChat")
        this.printChatConversation();
      else if (data.msg == "recentHistory")
        this.printRecentChatHistory();
      this.bsModalRef3.hide();
    });
  }


  onClickSelectedLanguage() {
    console.log("Selected Lanugae: " + this.selected_language);
  }

}
