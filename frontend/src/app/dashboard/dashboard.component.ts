import { Component, OnInit } from '@angular/core';
import { DbService } from '../service/db.service';
import { Papa } from 'ngx-papaparse';
import * as saveAs from 'file-saver';
import { TrainingData } from '../models/TrainingData.model';
import { Misc } from '../service/Misc';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { AddTrainingDataComponent } from '../add-training-data/add-training-data.component';
import { NluService } from '../service/nlu.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  trainingDataArr = [];

  //flags
  isTrainingDataUpdated = false;

  //html specific 
  searchFeedbackType = "All";
  searchAnswerType = "All";

  //datepicker
  searchStartTimeString: any;
  searchEndTimeString: any;

  constructor(private _dbservice: DbService, private _nluservice: NluService, private _papaParser: Papa, private modalService: BsModalService) { }

  ngOnInit() {
    this.resetHtml();
    this.populatetrainingDataArr();
    let htmlDate = new Date(Date.now());
    console.log(htmlDate);
  }

  resetHtml() {
    console.log("Resetting HTML..");
    this.searchStartTimeString = null;
    this.searchEndTimeString = null;
    this.searchFeedbackType = "All";
    this.searchAnswerType = "All";
    this.isTrainingDataUpdated = false;
  }

  async populatetrainingDataArr() {
    this.resetHtml();
    this._dbservice.getAllTrainingData().subscribe(
      data => {
        console.log("Got Training data from db... ");
        console.log(data);
        this.trainingDataArr = data['data'];
      },
      error => {
        console.log("error....");
        console.log(error);
      }
    )
  }

  //search 
  onClickSearchItems() {
    console.log("feedback: " + this.searchFeedbackType);
    console.log("answer: " + this.searchAnswerType);
    console.log("searchStartTimeString: " + this.searchStartTimeString);
    console.log("searchEndTimeString: " + this.searchEndTimeString);


    //check the search params
    var searchStartTimeNumber = (this.searchStartTimeString == null || this.searchStartTimeString == undefined) ? -1 : Misc.getTimeStamp(this.searchStartTimeString);
    var searchEndTimeNumber = (this.searchEndTimeString == null || this.searchEndTimeString == undefined) ? -1 : Misc.getTimeStamp(this.searchEndTimeString);
    console.log("searchStartTimeNumber: " + searchStartTimeNumber);
    console.log("searchEndTimeNumber: " + searchEndTimeNumber);

    var searchFeedbackTypeNumber = -1;
    if (this.searchFeedbackType == "Like") searchFeedbackTypeNumber = 1;
    if (this.searchFeedbackType == "Dislike") searchFeedbackTypeNumber = 0;
    var searchAnswerTypeNumber = -1;
    if (this.searchAnswerType == "Answered") searchAnswerTypeNumber = 1;
    if (this.searchAnswerType == "Unanswered") searchAnswerTypeNumber = 0;

    //modify search start and end time number
    if (searchStartTimeNumber != -1) searchStartTimeNumber = Misc.initSearchStartTime(searchStartTimeNumber);
    if (searchEndTimeNumber != -1) searchEndTimeNumber = Misc.initSearchEndTime(searchEndTimeNumber);

    this._dbservice.getSpecificTrainingData(searchStartTimeNumber, searchEndTimeNumber, searchFeedbackTypeNumber, searchAnswerTypeNumber).subscribe(
      data => {
        console.log("success: data=");
        console.log(data);
        this.trainingDataArr = data['data'];
      },
      err => {
        console.log("Error:");
        console.log(err);
      }
    )
  }

  //delete training data
  async onClickDeleteTrainingData(event) {
    console.log("Training Data to be deleted: " + event.target.value);
    var result = confirm("You really want to delete?");
    if (result) {
      this._dbservice.deleteTrainingData({ _id: event.target.value }).subscribe(
        data => {
          console.log("Training Data deleted. Now fetching fresh data : " + data);
          //train NLU
          this.askNLUToRetrain();
          //populate training data
          this.populatetrainingDataArr();
        },
        err => {
          console.log("error: ");
          console.log(err);
        }
      )
    } else {
      console.log("nnnnnnnnnnnn");
    }
  }


  onClickReset() {
    this.resetHtml();
  }

  //download training data to csv file
  onClickDownloadTrainingData() {
    console.log("about to save file ....");
    var fileContents = this._papaParser.unparse(this.getDownloadSpecificRecords());
    var file = new Blob([fileContents], { type: "text/plain;charset=utf-8" });
    let fileNameToSave = "dashboard_logs.csv";
    saveAs(file, fileNameToSave);
  }

  getDownloadSpecificRecords() {
    // let tmpItemLogsArr = this.itemLogsArr;
    let tmpTrainingDataArr = [];
    for (let k = 0; k < this.trainingDataArr.length; k++) {
      var x = {
        "log_date": new Date(this.trainingDataArr[k].log_date),
        "question": this.trainingDataArr[k].question,
        "answer": this.trainingDataArr[k].answer,
        "feedback": this.trainingDataArr[k].feedback,
        "sme": this.trainingDataArr[k].sme
      }
      tmpTrainingDataArr.push(x);
    }
    return tmpTrainingDataArr;
  }

  //on clickeing the pencil update 
  onClickUpdatePencil(trainingData) {
    console.log("This log will be updated... ");
    console.log(trainingData);
  }

  modifyTrainingData(event) {
    //call to http service
    console.log('clicked... : ' + event);
    console.log(this.trainingDataArr);
    this.isTrainingDataUpdated = true;
  }

  //save and retrain data
  onClickSaveRetrain() {
    // this.saveAndRetrainInNLU();
    this.saveAndRetrainInDB();
  }

  saveAndRetrainInDB() {
    // alert("This functionality is yet to be implemented");
    this._dbservice.updateManyTrainingData(this.trainingDataArr).subscribe(
      data => {
        console.log(data);
        this.populatetrainingDataArr().then(reply => {
          // alert("Training Data got updated in DB successfully.");
          console.log("Training Data got updated in DB successfully.");
        });
        this.askNLUToRetrain();
      },
      err => {
        console.log(err);
        alert("Error in updating Data");
      }
    )
  }



  askNLUToRetrain() {
    // alert("This functionality is yet to be implemented");
    this._nluservice.askNLUToRetrain().subscribe(
      data => {
        console.log("#### Retrain Response from NLU ####");
        console.log(data);
        alert("Data trained in NLU");
        this.populatetrainingDataArr().then(reply => {

        });
      },
      err => {
        console.log("#### Retrain ERROR Response from NLU ####");
        console.log(err);
        alert("Error in retraining Data '" + err + "'");
      }
    )
  }

  bsModalRef: BsModalRef;
  openModalWithComponent() {
    const initialState = {
      list: [
      ],
      title: 'Add Training Data'
    };
    this.bsModalRef = this.modalService.show(AddTrainingDataComponent, { initialState });
    console.log("dashboard: bsmodal:");
    console.log(this.bsModalRef);
    this.bsModalRef.content.closeBtnName = 'Close';

    this.bsModalRef.content.event.subscribe(data => {
      console.log('Child component\'s event was triggered', data);
      this.bsModalRef.hide();
      if (data.msg == "add") {
        this.populatetrainingDataArr();
        //ask NLU to train.
        this.askNLUToRetrain();
      }
    });
  }


  //File Upload
  ///////////////////////////////////////////////////////////
  //upload training data
  currentUploadFile = null;
  onClickUploadFile(event: any) {
    if (event.target.files && event.target.files.length > 0) {
      this.currentUploadFile = event.target.files[0];
    }
  }

  async onClickUploadTrainingData() {
    if (this.currentUploadFile == null) {
      alert("Kindly first choose the file to upload.");
      return;
    }

    await this.parseCSVAndAddTrainingData(this.currentUploadFile);

    //ask NLU to retrain the model
    await this.askNLUToRetrain();
    // await this.parseAndTrain(this.currentUploadFile);

    alert("Training Data successfully uploaded.");
  }


  // async parseAndTrain(csvFileToParse: File) {
  //   let misc = new Misc();
  //   let tmpTrainingDataArr = await (new Misc()).parseCSVAndReturnObjectArray(csvFileToParse, 1);
  //   console.log("Got from misc:");
  //   console.log(tmpTrainingDataArr);
  //   await this._dbservice.createTrainingDataFromUpload(tmpTrainingDataArr).subscribe(
  //     data => {
  //       console.log("Got reply from server.... .... ");
  //       console.log(data);
  //       //this.trainingDataArr = data['data'];
  //       this.populatetrainingDataArr();
  //     },
  //     err => {
  //       console.log("Got error.... " + err);
  //     }
  //   );


  // }


  /**
   * Parse and add Incentive csv file with following conitions on the file and is using PapaParser    
  */
  async parseCSVAndAddTrainingData(csvFileToParse: File) {
    console.log("parseCSV. Ready to parse file: " + csvFileToParse.name);

    let options = {
      complete: (results, file) => {
        // console.log('Parsed: ', results, file);
        // console.log("File:");
        // console.log(file);
        console.log("Results:");
        console.log(results);
        console.log(results.data.length);

        let tmpTrainingDataArr: Array<TrainingData> = [];
        let lineNo = 0;
        for (let res of results.data) {
          // console.log("RES:" + lineNo);
          // console.log(res);
          //ignore the first line as it would be heading          
          lineNo++;
          if (lineNo == 1) continue;//this is the header so ingnore it


          //if answer is not availbale for a particular question then skip such rows
          // console.log("2nd res: " + res[1]);
          if (res[1] == null || res[1] == "") {
            console.log("Skipping question: " + res[1]);
            continue;
          }

          //create incentive objects
          let trainingData = new TrainingData();
          trainingData.question = res[0];
          trainingData.answer = res[1];
          if (res[2] != null && res[2] != "") trainingData.feedback = res[2];
          if (res[3] != null && res[3] != "") trainingData.sme = res[3];
          if (res[4] != null && res[4] != "") trainingData.is_answered = res[4].toLowerCase() == 'true' ? true : false;
          if (res[5] != null && res[5] != "") trainingData.is_liked = res[5].toLowerCase() == 'true' ? true : false;
          tmpTrainingDataArr.push(trainingData);
        }

        //print all incentive
        // console.log("All item logs.... ")
        // console.log(tmpTrainingDataArr);

        this._dbservice.createTrainingDataFromUpload(tmpTrainingDataArr).subscribe(
          data => {
            console.log("Got reply from server.... .... ");
            console.log(data);
            //this.trainingDataArr = data['data'];
            this.populatetrainingDataArr();
          },
          err => {
            console.log("Got error.... " + err);
          }
        );


      },
      // Add other options here
      skipEmptyLines: true
    };
    try {
      this._papaParser.parse(csvFileToParse, options);
      console.log("Success in parsing.");
    } catch (error) {
      console.log("Error in parsing....")
      console.log(error);
    }

  }

}//end of class

