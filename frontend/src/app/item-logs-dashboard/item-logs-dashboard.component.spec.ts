import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ItemLogsDashboardComponent } from './item-logs-dashboard.component';

describe('ItemLogsDashboardComponent', () => {
  let component: ItemLogsDashboardComponent;
  let fixture: ComponentFixture<ItemLogsDashboardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ItemLogsDashboardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ItemLogsDashboardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
