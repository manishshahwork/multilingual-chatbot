export class ItemLog {
    _id: string;
    log_date: number = Date.now();
    question: string;
    answer: string;
    feedback: string;
    sme: string;
    is_answered: boolean;
    is_liked: boolean;
}