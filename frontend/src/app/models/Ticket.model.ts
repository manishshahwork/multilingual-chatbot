export class Ticket {
    issue_id: string;
    issue_key: string;
    issue_type: string;
    project_key: string;
    project_lead: string;
    priority: string;
    resolution: string;
    assignee: string;
    reporter: string;
    creator: string;
    description: string;    
}