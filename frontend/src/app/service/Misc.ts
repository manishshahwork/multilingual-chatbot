import { Papa } from 'ngx-papaparse';
import * as saveAs from 'file-saver';
import { TrainingData } from '../models/TrainingData.model';

export class Misc {
    _papaParser: Papa;
    constructor() {

    }

    //get timestamp from ngb date picker
    static getTimeStamp(str) {
        return new Date(str).getTime();
    }

    //init start time to first second of day
    static initSearchStartTime(ts) {
        let d = new Date(ts);
        d.setHours(0);
        d.setMinutes(0);
        d.setSeconds(0);
        return d.getTime();
    }

    //init end time to last second of day
    static initSearchEndTime(ts) {
        let d = new Date(ts);
        d.setHours(23);
        d.setMinutes(59);
        d.setSeconds(59);
        return d.getTime();
    }

    /**
    * Parse and add Incentive csv file with following conitions on the file and is using PapaParser    
   */
    // async parseCSVAndReturnObjectArray(csvFileToParse: File, returnObjectType): any {
    //     console.log("parseCSV. Ready to parse file: " + csvFileToParse.name);
    //     let options = {
    //         complete: (results, file) => {
    //             console.log('Parsed: ', results, file);
    //             console.log("File:");
    //             console.log(file);
    //             console.log("Results:");
    //             console.log(results);
    //             console.log(results.data.length);

    //             if (returnObjectType == 1) {
    //                 //training data
    //                 let tmpTrainingDataArr: Array<TrainingData> = [];
    //                 let lineNo = 0;
    //                 for (let res of results) {
    //                     console.log("RES:" + lineNo);
    //                     console.log(res);
    //                     //ignore the first line as it would be heading          
    //                     lineNo++;
    //                     if (lineNo == 1) continue;//this is the header so ingnore it

    //                     //create incentive objects
    //                     let trainingData = new TrainingData();
    //                     trainingData.question = res[0];
    //                     trainingData.answer = res[1];
    //                     trainingData.feedback = res[2];
    //                     trainingData.sme = res[3];
    //                     trainingData.is_answered = res[4];
    //                     trainingData.is_liked = res[5];

    //                     console.log("dd: " + new Date(trainingData.log_date));

    //                     tmpTrainingDataArr.push(trainingData);
    //                 }
    //                 return tmpTrainingDataArr;
    //             } else {

    //             }

    //             return results.data;
    //         },
    //         // Add other options here
    //         skipEmptyLines: true
    //     };

    //     try {
    //         // let _papaParser:Papa;
    //         this._papaParser = new Papa();
    //         this._papaParser.parse(csvFileToParse, options);
    //         console.log("Success in parsing.");
    //     } catch (error) {
    //         console.log("Error in parsing....")
    //         console.log(error);
    //     }

    // }
}