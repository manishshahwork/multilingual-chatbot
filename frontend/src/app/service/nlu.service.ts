import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { TrainingData } from '../models/TrainingData.model';

@Injectable({
  providedIn: 'root'
})
export class NluService {

  // NLU_URL = "http://ec2-13-233-138-244.ap-south-1.compute.amazonaws.com:3001/api";
  NLU_URL = "http://ec2-13-234-78-184.ap-south-1.compute.amazonaws.com:3001/api";

  constructor(private _http: HttpClient) { }

  getAnswerForQuestion(question: string, language: string = "english") {
    console.log("#### Will call NLU Service to get response for this question: '" + question + "' in this language:'" + language + "'");
    console.log(question);
    let q = this.NLU_URL + "?query=" + question + "&language=" + language + ""; 
    console.log("#### Query URL to Access :" + q);
    let questionObj = { "query": question }; //not needed as we are populating the query with params directly
    return this._http.post(q, questionObj);
  }

  askNLUToRetrain() {
    console.log("#### Will call NLU Service to retrain the data: ");
    let url = this.NLU_URL + "/retrainmodel";
    console.log("#### Retrain URL: " + url);
    return this._http.get(url);
  }
}