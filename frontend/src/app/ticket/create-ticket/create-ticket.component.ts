import { Component, OnInit, EventEmitter } from '@angular/core';
import { Ticket } from 'src/app/models/Ticket.model';

@Component({
  selector: 'app-create-ticket',
  templateUrl: './create-ticket.component.html',
  styleUrls: ['./create-ticket.component.css']
})
export class CreateTicketComponent implements OnInit {

  public event: EventEmitter<any> = new EventEmitter();
  ticket: Ticket;

  constructor() { }

  ngOnInit() {
    this.ticket = new Ticket();
  }

  onClickAddTrainingData(){
    console.log(this.ticket);
    // this._db.addTrainingData(this.trainingData).subscribe(
    //   data=>{
    //     console.log("Successfully added training data");
    //     this.event.emit({msg: "add"});
    //   },
    //   err =>{
    //     console.log("Error in adding training data.");
    //     console.log(err);
    //   }
    // );    
  }

  onClickCancel(){
    console.log(this.event);
    this.event.emit({data: "cancel"});
  }

}
