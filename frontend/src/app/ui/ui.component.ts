import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ChatbotComponent } from '../chatbot/chatbot.component';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';

@Component({
  selector: 'app-ui',
  templateUrl: './ui.component.html',
  styleUrls: [
    './ui.component.css'
  ]
})
export class UiComponent implements OnInit {
  closeResult: string;
  constructor(private _router: Router, private modalService: BsModalService) { }

  ngOnInit() {
  }


  bsModalRef: BsModalRef;
  openModalWithComponent() {
    const initialState = {
      list: [
      ],
      title: 'Quick Support'
    };
    this.bsModalRef = this.modalService.show(ChatbotComponent, { initialState });
    console.log("uiLbsmodalref: ");
    console.log(this.bsModalRef);
    console.log("uiL bsmodalref: " + this.bsModalRef.content.eventEmitter);
    this.bsModalRef.content.closeBtnName = 'Close';

    this.bsModalRef.content.eventEmitter.subscribe(data => {
      console.log('Child component\'s event was triggered', data);
      this.bsModalRef.hide();
    });
  }
}
